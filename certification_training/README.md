# Exercises for the `dbt` Analytics Engineering Certification (AE) Exam

## Sample Questions to train `dbt` skill & prepare for the AE Certification Exam

In similar fashion as the [CKAD-exercises](https://github.com/dgkanatsios/CKAD-exercises), we have created exercises based on the official dbt AE [Study Guide](https://www.getdbt.com/assets/uploads/dbt_certificate_study_guide.pdf).

## Exercises for each Exam Topic

- [Topic 1: Developing dbt models](topic_1_developing_dbt_models.md)
- [Topic 2: Debugging data modeling errors](topic_2_debugging_data_modeling_errors.md)
- [Topic 3: Monitoring data pipelines](topic_3_monitoring_data_pipelines.md)
- [Topic 4: Implementing dbt tests](topic_4_implementing_dbt_tests.md)
- [Topic 5: Deploying dbt jobs](topic_5_deploying_dbt_jobs.md)
- [Topic 6: Creating and Maintaining](topic_6_creating_and_maintining_dbt_documentation.md)
- [Topic 7: Promoting code through](topic_7_promoting_code_through_version_control.md)
- [Topic 8: Establishing environments in data](topic_8_establishing_environments_in_data_warehouse_for_dbt.md)









