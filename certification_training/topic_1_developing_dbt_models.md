### Sample Graph for Exercise

![Sample Graph](images/sample_graph.png)

### Given the `Sample Graph`, identify the sub-graphs using `dbt` graph notation

Using `dbt`'s [Graph-Operator](https://docs.getdbt.com/reference/node-selection/graph-operators) notation, identify sub-graphs, which would be activated by the `dbt ls --select` command, for;

1. `+attributes_count`
2. `+int_superhero_attributes+`
3. `stg_colour+1`
4. `stg_superhero`
5. `stg_superhero@`
6. `@int_superheroes`
7. `"superheroes.*"`
8. `stg_colour+1 1+stg_superhero`
9. `+attributes_count,@int_superheroes`

<details><summary>show</summary>
<p>

```bash
dbt ls --select +attributes_count
superheroes.marts.attributes_count
superheroes.staging.superheroes.stg_attribute
superheroes.staging.superheroes.stg_hero_attributes
superheroes.staging.superheroes.stg_publisher
superheroes.staging.superheroes.stg_superhero
source:superheroes.superheroes.attribute
source:superheroes.superheroes.hero_attribute
source:superheroes.superheroes.publisher
source:superheroes.superheroes.superhero

dbt ls --select +int_superhero_attributes+
superheroes.marts.heroes_overview
superheroes.intermediate.int_superhero_attributes
superheroes.staging.superheroes.stg_attribute
superheroes.staging.superheroes.stg_hero_attributes
superheroes.staging.superheroes.stg_superhero
source:superheroes.superheroes.attribute
source:superheroes.superheroes.hero_attribute
source:superheroes.superheroes.superhero

dbt ls --select stg_colour+1              
superheroes.intermediate.int_superheroes
superheroes.staging.superheroes.stg_colour

dbt ls --select stg_superhero
superheroes.staging.superheroes.stg_superhero

dbt ls --select stg_superhero@
XX:XX:XX  The selection criterion 'stg_superhero@' does not match any nodes
XX:XX:XX  No nodes selected!

dbt ls --select @int_superheroes
superheroes.marts.heroes_overview
superheroes.intermediate.int_superhero_attributes
superheroes.intermediate.int_superheroes
superheroes.staging.superheroes.stg_alignment
superheroes.staging.superheroes.stg_attribute
superheroes.staging.superheroes.stg_colour
superheroes.staging.superheroes.stg_gender
superheroes.staging.superheroes.stg_hero_attributes
superheroes.staging.superheroes.stg_race
superheroes.staging.superheroes.stg_superhero
source:superheroes.superheroes.alignment
source:superheroes.superheroes.attribute
source:superheroes.superheroes.colour
source:superheroes.superheroes.gender
source:superheroes.superheroes.hero_attribute
source:superheroes.superheroes.race
source:superheroes.superheroes.superhero

dbt ls --select "superheroes.*"
superheroes.marts.attributes_count
superheroes.marts.heroes_overview
superheroes.intermediate.int_superhero_attributes
superheroes.intermediate.int_superheroes
superheroes.staging.superheroes.stg_alignment
superheroes.staging.superheroes.stg_attribute
superheroes.staging.superheroes.stg_colour
superheroes.staging.superheroes.stg_dates
superheroes.staging.superheroes.stg_gender
superheroes.staging.superheroes.stg_hero_attributes
superheroes.staging.superheroes.stg_hero_power
superheroes.staging.superheroes.stg_publisher
superheroes.staging.superheroes.stg_race
superheroes.staging.superheroes.stg_superhero
superheroes.staging.superheroes.stg_superpower
superheroes.alignment
superheroes.attribute
superheroes.colour
superheroes.gender
superheroes.hero_attribute
superheroes.hero_power
superheroes.publisher
superheroes.race
superheroes.superhero
superheroes.superpower

dbt ls --select stg_colour+1 1+stg_superhero 
superheroes.intermediate.int_superheroes
superheroes.staging.superheroes.stg_colour
superheroes.staging.superheroes.stg_superhero
source:superheroes.superheroes.superhero

dbt ls --select +attributes_count,@int_superheroes
superheroes.staging.superheroes.stg_attribute
superheroes.staging.superheroes.stg_hero_attributes
superheroes.staging.superheroes.stg_superhero
source:superheroes.superheroes.attribute
source:superheroes.superheroes.hero_attribute
source:superheroes.superheroes.superhero


```

</p>
</details>

### Sample Directory Stucture

```
models
├── intermediate
│ ├── int_superhero_attributes.sql
│ └── int_superheroes.sql
├── marts
│ ├── attributes_count.sql
│ └── heroes_overview.sql
└── staging
    └── superheroes
        ├── src_superheroes.yml
        ├── stg_alignment.sql
        ├── stg_attribute.sql
        ├── stg_colour.sql
        ├── stg_gender.sql
        ├── stg_hero_attributes.sql
        ├── stg_hero_power.sql
        ├── stg_publisher.sql
        ├── stg_race.sql
        ├── stg_superhero.sql
        └── stg_superpower.sql
```

### Given the directory Structure within `/models`, (used to create above sample graph), making use of the directory structure, what command would run all staging `stg_` models?

<details><summary>show</summary>
<p>

```bash
dbt run --select staging
```

</p>
</details>

### dbt its able to construct the model dependencie graph based on...

- **A**: Using the `ref()` function
- **B**: Using its adapters *SQL-engine*
- **C**: Parsing the output from *dbt compile* 
- **D**: Parsing the output from *dbt source*

<details><summary>show</summary>
<p>

**A:** See the [ref()](https://docs.getdbt.com/reference/dbt-jinja-functions/ref) documentation.

</p>
</details>

### Name the 4 types of dbt materializations, built into dbt

<details><summary>show</summary>
<p>

- table
- view
- incremental
- ephemeral

</p>
</details>

### Fill in the blanks. The [...] materializations is used insert or update records into a table since the last time that dbt was run. 

<details><summary>show</summary>
<p>

[Incremental](https://docs.getdbt.com/docs/build/materializations#incremental)

</p>
</details>


### Fill in the blanks. [...] models are not directly built into the database. Instead, dbt will interpolate the code from this model into dependent models as a common table expression.

<details><summary>show</summary>
<p>

[Ephemeral](https://docs.getdbt.com/docs/build/materializations#ephemeral)

</p>
</details>

### Using the `ref()` function, fill in the [...] to use the `stg_superhero` table, in the following CTE snippet

```sql
with superheroes as (

    select * from ...

),

```

<details><summary>show</summary>
<p>

```
 {{ ref('stg_superhero') }}
```

</p>
</details>

### Enter the two commands to generate and serve the dbt project's documentation.

<details><summary>show</summary>
<p>

```bash
dbt docs generate
dbt docs serve
```

</p>
</details>

### Enter the command to upload the `.csv` files into the data warehouse with `dbt`, using the optional argument to display a sample for each of the uploaded `.csv`'s.

<details><summary>show</summary>
<p>

```bash
dbt seed --show
```

</p>
</details>

### Mark the following statements about a dbt project & `dbt_project.yml` project file as **true** or **false**;

1. The `dbt_project.yml` file is how dbt knows a directory is a dbt project.
2. The Datawarehouse access credentials are stored in the `dbt_project.yml` file.
3. The `/models` directory contains your dbt models, and cannot be altered.
4. Variables can be configured in the `dbt_project.yml` project file, to be used in models with Jinja templating.
5. The project name is defined by the name of the directory your `dbt_project.yml` lives in.
6. A dbt profile, defines how your dbt project connects to the data warehouse.
7. The best and recommended way to have seperated `dev`, `prod`, and possibly `accept` environment, is by using different profiles
8. The number of `threads` dbt runs on is defined in you `dbt_project.yml` 

<details><summary>show</summary>
<p>

1. [True](https://docs.getdbt.com/reference/dbt_project.yml)
2. [False](https://docs.getdbt.com/docs/get-started/connection-profiles), these are stored in profiles.
3. [False](https://docs.getdbt.com/docs/build/projects#project-configuration), `model-paths` within you `dbt_project.yml` can be used to define the path where your models are.
4. [True](https://docs.getdbt.com/docs/build/project-variables)
5. [False](https://docs.getdbt.com/docs/build/projects#project-configuration), its within the `dbt_project.yml`, at the key `name:`
6. [True](https://docs.getdbt.com/docs/get-started/connection-profiles)
7. [False](https://docs.getdbt.com/docs/get-started/connection-profiles#setting-up-your-profile), using **targets** within a **profile**, is the preferred, supported, and documented way to use different environments such as `dev` & `prod`.
8. [False](https://docs.getdbt.com/docs/get-started/connection-profiles#setting-up-your-profile), threads are defined in the profile.

</p>
</details>

### Within a source named `games`, a table `nintendo` is defined. Fill in the blanks to complete the following code snippet; `select * from {{...}}`, referencing to the source table.

<details><summary>show</summary>
<p>

```
{{ source('games', 'nintendo') }}
```


</p>
</details>


### What command can be used to check the freshness of your source...

<details><summary>show</summary>
<p>


```bash
dbt source freshness
```


</p>
</details>


### Mark the following statements about a dbt sources as **true** or **false**;

1. Sources could be from a different **Database**.
2. If `loaded_at_field` is not provided, Source freshness will be determined with table meta-data.
3. The `identifier` of a source, functions as a alias, to be used with `source('jaffle_shop', 'orders')`.
4. The optional `loader` property for a source, is adapter specific performance tuning measure.
5. If no `schema` is provided for a source, dbt will use the source's name for its schema.
6. Sources, like Models, can be tagged, to later by used with `--select` flags for `run` or `test`.

<details><summary>show</summary>
<p>

1. [True](https://docs.getdbt.com/reference/resource-properties/database#definition)
2. [False](https://docs.getdbt.com/reference/resource-properties/freshness#definition)
3. [False](https://docs.getdbt.com/reference/resource-properties/identifier#definition), the **name** is what is the argument for the `source()`, and the optional `identifier` can be used to refer to a table than **name**.
4. [False](https://docs.getdbt.com/reference/resource-properties/loader#definition), This is for documentation, to refer to loaders as fivetran or airbyte, merely for documentation purposes.
5. [True](https://docs.getdbt.com/reference/resource-properties/schema#definition)
6. [True](https://docs.getdbt.com/reference/resource-configs/tags), and tags can be applied on more finegraned table and/or column level.

</p>
</details>
