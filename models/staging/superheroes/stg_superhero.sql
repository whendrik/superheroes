select 
    id as hero_id,
    superhero_name,
    full_name,
    race_id,
    publisher_id,
    gender_id,
    eye_colour_id,
    hair_colour_id,
    alignment_id,
    height_cm,
    weight_kg
    
from {{ ref('superhero') }}