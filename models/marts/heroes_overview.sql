with superheroes as (

    select * from {{ ref('int_superheroes')}}

),

superheroes_attributes as (

    select * from {{ ref('int_superhero_attributes')}}

),

final as (

    select
        superheroes.superhero_name as name,
        superheroes.alignment.alignment as alignment,
        superheroes.gender.gender as gender,
        superheroes.eye_colour.colour as eye_colour,
        superheroes.race.race as race,
        {% set attributes = ['Intelligence', 'Strength', 'Speed', 'Durability', 'Power', 'Combat'] -%}
        {% for attr in attributes -%}
        superheroes_attributes.{{ attr }} as {{ attr }},
        {% endfor %}

    from superheroes

    left join superheroes_attributes on superheroes_attributes.hero_id = superheroes.hero_id
)

select * from final
