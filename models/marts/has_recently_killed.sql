with superheroes as (

    select * from {{ ref('int_superheroes')}}

),

final as (

    select
        superheroes.hero_id as hero_id,
        superheroes.superhero_name,
        superheroes.alignment.alignment as alignment,
        (rand() < 0.05) as recently_killed 

    from superheroes
)

select * from final
