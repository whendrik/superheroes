with superheroes as (
    select * from {{ ref('stg_superhero')}}
),

publisher as (
    select * from {{ ref('stg_publisher')}}
),

hero_attribures as (
    select * from {{ ref("stg_hero_attributes") }}
),

attribute as (
    select * from {{ ref("stg_attribute") }}
),

final as (

    select
        superheroes.hero_id as hero_id,
        superheroes.superhero_name as name,
        publisher.publisher_name as publisher_name,
        SUM(hero_attribures.attribute_value) as total_attributes

    from superheroes

    inner join publisher on publisher.publisher_id = superheroes.publisher_id
    left join hero_attribures ON superheroes.hero_id = hero_attribures.hero_id
    left join attribute ON hero_attribures.attribute_id = attribute.attribute_id
    group by hero_id, name, publisher_name
    order by total_attributes desc
)

select * from final
