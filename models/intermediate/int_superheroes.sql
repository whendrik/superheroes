with superhero as (
    select * from {{ ref('stg_superhero') }}
),

alignment as (
    select * from {{ ref('stg_alignment')}}
),

colour as (
    select * from {{ ref('stg_colour')}}
),

gender as (
    select * from {{ ref('stg_gender') }}
),

race as (
    select * from {{ ref('stg_race') }}
),

joined as (

    select
        superhero.hero_id,
        superhero.superhero_name,
        alignment.alignment,
        eye_colour.colour as eye_colour,
        gender.gender as gender,
        race.race

    from superhero

    left join alignment on alignment.alignment_id = superhero.alignment_id
    left join colour as eye_colour on eye_colour.colour_id = superhero.eye_colour_id 
    left join gender on gender.gender_id = superhero.gender_id
    left join race on race.race_id = superhero.race_id

)

select * from joined
