with superhero as (
    select * from {{ ref('stg_superhero') }}
),

hero_attributes as (
    select * from {{ ref("stg_hero_attributes") }}
),

attribute as (
    select * from {{ ref("stg_attribute") }}
),


{%- set attributes = ['Intelligence', 'Strength', 'Speed', 'Durability', 'Power', 'Combat'] -%}

joined as (

    select
        superhero.hero_id,
        {% for attr in attributes -%}
        sum(case when attribute_name = "{{ attr }}" then attribute_value else 0 end) 
            as {{ attr }}
        {%- if not loop.last -%}
        ,
        {% endif -%}
        {%- endfor %}

    from superhero

    left join hero_attributes ON superhero.hero_id = hero_attributes.hero_id
    left join attribute ON hero_attributes.attribute_id = attribute.attribute_id

    group by superhero.hero_id
)

select * from joined
