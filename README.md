# Exercise Project for the `dbt` Analytics Engineering (AE) Certification Exam

The Official Free [dbt courses](https://courses.getdbt.com/) contain many learning material. This repo provides you even more material with the goal to master `dbt` & prepare you for the Analytics Engineering Certification Exam.

The project uses [dbt core](https://docs.getdbt.com/docs/get-started/getting-started-dbt-core) in combination with [BigQuery](https://docs.getdbt.com/docs/get-started/getting-started/getting-set-up/setting-up-bigquery). 

To follow & repeat all the exercises;

1. [Install dbt](https://docs.getdbt.com/docs/get-started/installation) with the BigQuery Adapter
2. [Setup dbt with BigQuery](https://docs.getdbt.com/docs/get-started/getting-started/getting-set-up/setting-up-bigquery)
3. Clone this repo

All exercises are Super Hero themed, using the [databasestar](https://www.databasestar.com/), example data.

![superheroes](images/table.png)

### Original Data Source - Credits to databasestar.com

Credits to [databasestar](https://www.databasestar.com/), for creating realistic & fun data for `SQL` exercises.
We used the [SuperHeroes](https://www.databasestar.com/sample-data-sql/#Superheroes) data, and converted them to work with [GCP Big Query](https://cloud.google.com/bigquery)

All `.SQL` DDL's are converted to `.csv`, to be compatible with BigQuery using the [dbt seed](https://docs.getdbt.com/docs/build/seeds) command.

### Sample Project

#### 01 - Initialize the Source data with `dbt seed`

Use 

```bash
dbt seed
```

to create the (source) tables.

### Exercises for the `dbt` Analytics Engineering Certification (AE) Exam

Exercises for the `dbt` Analytics Engineering Certification (DAE) Exam, are within the
[/certification_training](/certification_training)


- [Topic 1: Developing dbt models](certification_training/topic_1_developing_dbt_models.md)
- [Topic 2: Debugging data modeling errors](certification_training/topic_2_debugging_data_modeling_errors.md)
- [Topic 3: Monitoring data pipelines](certification_training/topic_3_monitoring_data_pipelines.md)
- [Topic 4: Implementing dbt tests](certification_training/topic_4_implementing_dbt_tests.md)
- [Topic 5: Deploying dbt jobs](certification_training/topic_5_deploying_dbt_jobs.md)
- [Topic 6: Creating and Maintaining](certification_training/topic_6_creating_and_maintining_dbt_documentation.md)
- [Topic 7: Promoting code through](certification_training/topic_7_promoting_code_through_version_control.md)
- [Topic 8: Establishing environments in data](certification_training/topic_8_establishing_environments_in_data_warehouse_for_dbt.md)
